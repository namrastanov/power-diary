﻿using ChatApp.Core.ReadModels;
using ChatApp.Data.Enums;
using System.Collections.Generic;
using System;
using ChatApp.Core.Exceptions;

namespace ChatApp.Core.Factories
{
    public class RoomEventFactory
    {
        public static Dictionary<RoomEventType, Func<string, PersonReadModel, RoomEventReadModel>> Creators =
            new Dictionary<RoomEventType, Func<string, PersonReadModel, RoomEventReadModel>>
            {
                { RoomEventType.EnterTheRoom, (commentText, highFivedPerson) => new EnterEvent() },
                { RoomEventType.LeaveTheRoom, (commentText, highFivedPerson) => new LeaveEvent() },
                { RoomEventType.HighFive, (commentText, highFivedPerson) => CreateHighFiveEvent(highFivedPerson) },
                { RoomEventType.Comment, (commentText, highFivedPerson) => CreateCommentEvent(commentText) }
            };

        private static HighFiveEvent CreateHighFiveEvent(PersonReadModel highFivedPerson)
        {
            if (highFivedPerson == null)
            {
                throw new CustomException("High fived person is NULL");
            }

            return new HighFiveEvent(highFivedPerson);
        }

        private static CommentEvent CreateCommentEvent(string commentText)
        {
            if (commentText == null)
            {
                throw new CustomException("Comment text is NULL");
            }
            return new CommentEvent(commentText);
        }
    }
}

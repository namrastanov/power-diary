﻿using ChatApp.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ChatApp.Core
{
    public static class Dependencies
    {
        public static ServiceCollection InitCoreDependencies(this ServiceCollection _this)
        {
            return (ServiceCollection)_this.AddSingleton<IRoomHistoryService, RoomHistoryService>();
        }
    }
}

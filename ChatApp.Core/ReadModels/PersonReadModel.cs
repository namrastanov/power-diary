﻿using System;

namespace ChatApp.Core.ReadModels
{
    public class PersonReadModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

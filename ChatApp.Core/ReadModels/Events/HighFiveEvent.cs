﻿using System;

namespace ChatApp.Core.ReadModels
{
    public class HighFiveEvent: RoomEventReadModel
    {
        public HighFiveEvent(PersonReadModel highFivedPerson)
        {
            HighFivedPerson = highFivedPerson;
        }

        public PersonReadModel HighFivedPerson { get; set; }
    }
}

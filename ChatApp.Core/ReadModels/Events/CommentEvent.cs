﻿using System;

namespace ChatApp.Core.ReadModels
{
    public class CommentEvent: RoomEventReadModel
    {
        public CommentEvent(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }
}

﻿using System;

namespace ChatApp.Core.ReadModels
{
    public abstract class RoomEventReadModel
    {
        public Guid Id { get; set; }
        public Guid RoomId { get; set; }
        public DateTime DateTime { get; set; }
        public PersonReadModel Person { get; set; }
    }
}

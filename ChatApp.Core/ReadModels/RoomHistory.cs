﻿using System.Collections.Generic;

namespace ChatApp.Core.ReadModels
{
    public class RoomHistory
    {
        public IList<RoomEventReadModel> RoomEvents { get; set; }
    }
}

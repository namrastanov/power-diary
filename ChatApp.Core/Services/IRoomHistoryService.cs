﻿using ChatApp.Core.ReadModels;
using System;

namespace ChatApp.Core.Services
{
    public interface IRoomHistoryService
    {
        RoomHistory GetHistory(Guid roomId);
    }
}

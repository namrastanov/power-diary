﻿using ChatApp.Core.Constants;
using ChatApp.Core.Factories;
using ChatApp.Core.ReadModels;
using ChatApp.Data.Queries;
using System;
using System.Linq;

namespace ChatApp.Core.Services
{
    public class RoomHistoryService : IRoomHistoryService
    {
        private readonly IRoomEventQuery _roomEventQuery;

        public RoomHistoryService(
            IRoomEventQuery roomEventQuery)
        {
            _roomEventQuery = roomEventQuery;
        }

        public RoomHistory GetHistory(Guid roomId)
        {
            var roomHistory = new RoomHistory
            {
                RoomEvents = _roomEventQuery
                    .GetRoomEvents(roomId)
                    .Select(data =>
                    {
                        var roomEvent = RoomEventFactory.Creators[data.roomEvent.Type](
                            data.commentText,
                            AutomapperConfiguration.mapper.Map<PersonReadModel>(data.highFivedPerson));

                        AutomapperConfiguration.mapper.Map(data.roomEvent, roomEvent);
                        roomEvent.Person = AutomapperConfiguration.mapper.Map<PersonReadModel>(data.person);
                        return roomEvent;
                    })
                    .OrderBy(roomEvent => roomEvent.DateTime)
                    .ToList()
            };

            return roomHistory;
        }
    }
}

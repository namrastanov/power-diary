﻿using AutoMapper;
using ChatApp.Core.ReadModels;
using ChatApp.Data.Models;

namespace ChatApp.Core.Constants
{
    public class AutomapperConfiguration
    {
        public static IMapper mapper = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<RoomEvent, RoomEventReadModel>();
            cfg.CreateMap<Person, PersonReadModel>();
        }).CreateMapper();
    }
}

﻿using System;

namespace ChatApp.Data.Models
{
    public class Comment: Entity
    {
        public Guid RoomEventId { get; set; }
        public string Text { get; set; }
    }
}

﻿using ChatApp.Data.Enums;
using System;

namespace ChatApp.Data.Models
{
    public class RoomEvent: Entity
    {
        public Guid RoomId { get; set; }
        public RoomEventType Type { get; set; }
        public Guid PersonId { get; set; }
        public DateTime DateTime { get; set; }
    }
}

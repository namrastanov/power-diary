﻿using System;

namespace ChatApp.Data.Models
{
    public class HighFive: Entity
    {
        public Guid RoomEventId { get; set; }
        public Guid PersonId { get; set; }
    }
}

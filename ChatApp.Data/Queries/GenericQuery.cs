﻿using ChatApp.Data.Mocks;
using ChatApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChatApp.Data.Queries
{
    public class GenericQuery<T> : IGenericQuery<T> where T : Entity
    {
        public T GetById(Guid id)
        {
            return GetAll()?.FirstOrDefault(c => c.Id == id);
        }

        // we could optimize the mock's interaction to avoid if statements here
        // but I don't think it is important because basically in real world in common cases we are working with EF's DbSet here
        public IList<T> GetAll()
        {
            if (typeof(T) == typeof(Person))
            {
                return PersonsMock.Persons as IList<T>;
            }
            if (typeof(T) == typeof(Comment))
            {
                return CommentsMock.Comments as IList<T>;
            }
            if (typeof(T) == typeof(HighFive))
            {
                return HighFivesMock.HighFives as IList<T>;
            }
            if (typeof(T) == typeof(RoomEvent))
            {
                var result = RoomEventsMock_1.RoomEvents as List<T>;
                result.AddRange(RoomEventsMock_2.RoomEvents as List<T>);
                return result;
            }
            return null;
        }
    }
}

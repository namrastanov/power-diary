﻿using ChatApp.Data.Models;
using System;
using System.Collections.Generic;

namespace ChatApp.Data.Queries
{
    public interface IGenericQuery<T> where T: Entity
    {
        T GetById(Guid id);
        IList<T> GetAll();
    }
}

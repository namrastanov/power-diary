﻿using ChatApp.Data.Models;
using System;
using System.Collections.Generic;

namespace ChatApp.Data.Queries
{
    public interface IRoomEventQuery : IGenericQuery<RoomEvent>
    {
        IList<(RoomEvent roomEvent, string commentText, Person person, Person highFivedPerson)> GetRoomEvents(Guid roomId);
    }
}

﻿using ChatApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChatApp.Data.Queries
{
    public class RoomEventQuery : GenericQuery<RoomEvent>, IRoomEventQuery
    {
        private readonly IGenericQuery<Comment> _commentQuery;
        private readonly IGenericQuery<Person> _personQuery;
        private readonly IGenericQuery<HighFive> _highFiveQuery;

        public RoomEventQuery(
            IGenericQuery<Comment> commentQuery,
            IGenericQuery<Person> personQuery,
            IGenericQuery<HighFive> highFiveQuery)
        {
            _commentQuery = commentQuery;
            _personQuery = personQuery;
            _highFiveQuery = highFiveQuery;
        }

        // Used Tuple here to avoid adding new model or including the data model's references in the RoomEvent data model
        public IList<(RoomEvent roomEvent, string commentText, Person person, Person highFivedPerson)> GetRoomEvents(Guid roomId)
        {
            return GetAll()
                .Where(roomEvent => roomEvent.RoomId == roomId)
                .Select(roomEvent =>
                {
                    var comment = _commentQuery.GetAll().FirstOrDefault(c => c.RoomEventId == roomEvent.Id);
                    var person = _personQuery.GetById(roomEvent.PersonId);
                    var highFive = _highFiveQuery.GetAll().FirstOrDefault(h => h.RoomEventId == roomEvent.Id);
                    var highFivedPerson = highFive != null ? _personQuery.GetById(highFive.PersonId) : null;
                    return (roomEvent, comment?.Text, person, highFivedPerson);
                })
                .ToList();
        }
    }
}

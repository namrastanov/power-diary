﻿
using ChatApp.Data.Models;
using ChatApp.Data.Queries;
using Microsoft.Extensions.DependencyInjection;

namespace ChatApp.Data
{
    public static class Dependencies
    {
        public static ServiceCollection InitDataDependencies(this ServiceCollection _this)
        {
            return (ServiceCollection)_this
                .AddSingleton<IGenericQuery<Comment>, GenericQuery<Comment>>()
                .AddSingleton<IGenericQuery<HighFive>, GenericQuery<HighFive>>()
                .AddSingleton<IGenericQuery<Person>, GenericQuery<Person>>()
                .AddSingleton<IRoomEventQuery, RoomEventQuery>();
        }
    }
}

﻿namespace ChatApp.Data.Enums
{
    public enum RoomEventType
    {
        EnterTheRoom, LeaveTheRoom, Comment, HighFive
    }
}

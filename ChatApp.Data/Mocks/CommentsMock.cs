﻿using ChatApp.Data.Models;
using System;
using System.Collections.Generic;

namespace ChatApp.Data.Mocks
{
    public static class CommentsMock
    {
        public static IList<Comment> Comments = new List<Comment>
        {
            new Comment {Id = Guid.NewGuid(), RoomEventId = RoomEventsMock_1.Person_0_CommentEventId, Text = "Hey, Kate - high five?"},
            new Comment {Id = Guid.NewGuid(), RoomEventId = RoomEventsMock_1.Person_1_CommentEventId, Text = "Oh, typical"},

            new Comment {Id = Guid.NewGuid(), RoomEventId = RoomEventsMock_2.Person_0_CommentEventId, Text = "Sorry, I had to leave the chat."},
            new Comment {Id = Guid.NewGuid(), RoomEventId = RoomEventsMock_2.Person_1_CommentEventId, Text = "No problem."},
            new Comment {Id = Guid.NewGuid(), RoomEventId = RoomEventsMock_2.Person_1_Comment2EventId, Text = "It is not a big deal."},
            new Comment {Id = Guid.NewGuid(), RoomEventId = RoomEventsMock_2.Person_1_Comment3EventId, Text = "Bye."},
        };
    }
}

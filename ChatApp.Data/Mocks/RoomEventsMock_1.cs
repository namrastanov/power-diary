﻿using ChatApp.Data.Enums;
using ChatApp.Data.Models;
using System;
using System.Collections.Generic;

namespace ChatApp.Data.Mocks
{
    public static class RoomEventsMock_1
    {
        public static Guid Person_0_CommentEventId = Guid.NewGuid();
        public static Guid Person_1_CommentEventId = Guid.NewGuid();
        public static Guid Person_1_HighFiveEventId = Guid.NewGuid();

        public static IList<RoomEvent> RoomEvents = new List<RoomEvent>
        {
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[0].Id,
                Type = RoomEventType.EnterTheRoom,
                DateTime = DateTime.Now.AddHours(-1).AddMinutes(-7)
            },
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.EnterTheRoom,
                DateTime = DateTime.Now.AddHours(-1).AddMinutes(-6)
            },
            new RoomEvent {
                Id = Person_0_CommentEventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[0].Id,
                Type = RoomEventType.Comment,
                DateTime = DateTime.Now.AddHours(-1).AddMinutes(-5)
            },
            new RoomEvent {
                Id = Person_1_HighFiveEventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.HighFive,
                DateTime = DateTime.Now.AddHours(-1).AddMinutes(-4)
            },
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[0].Id,
                Type = RoomEventType.LeaveTheRoom,
                DateTime = DateTime.Now.AddHours(-1).AddMinutes(-3)
            },
            new RoomEvent {
                Id = Person_1_CommentEventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.Comment,
                DateTime = DateTime.Now.AddHours(-1).AddMinutes(-2)
            },
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.LeaveTheRoom,
                DateTime = DateTime.Now.AddHours(-1).AddMinutes(-1)
            }
        };
    }
}

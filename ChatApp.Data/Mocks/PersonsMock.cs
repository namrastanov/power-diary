﻿using ChatApp.Data.Models;
using System;
using System.Collections.Generic;

namespace ChatApp.Data.Mocks
{
    public static class PersonsMock
    {
        public static IList<Person> Persons = new List<Person>
        {
            new Person
            {
                Id = Guid.NewGuid(),
                Name = "Bob"
            },
            new Person
            {
                Id = Guid.NewGuid(),
                Name = "Kate"
            }
        };
    }
}

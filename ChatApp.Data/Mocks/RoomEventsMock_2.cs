﻿using ChatApp.Data.Enums;
using ChatApp.Data.Models;
using System;
using System.Collections.Generic;

namespace ChatApp.Data.Mocks
{
    public static class RoomEventsMock_2
    {
        public static Guid Person_0_CommentEventId = Guid.NewGuid();
        public static Guid Person_1_CommentEventId = Guid.NewGuid();
        public static Guid Person_1_Comment2EventId = Guid.NewGuid();
        public static Guid Person_1_Comment3EventId = Guid.NewGuid();
        public static Guid Person_1_HighFiveEventId = Guid.NewGuid();
        public static Guid Person_0_HighFiveEventId = Guid.NewGuid();

        public static IList<RoomEvent> RoomEvents = new List<RoomEvent>
        {
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[0].Id,
                Type = RoomEventType.EnterTheRoom,
                DateTime = DateTime.Now.AddMinutes(-10)
            },
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.EnterTheRoom,
                DateTime = DateTime.Now.AddMinutes(-9)
            },
            new RoomEvent {
                Id = Person_0_CommentEventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[0].Id,
                Type = RoomEventType.Comment,
                DateTime = DateTime.Now.AddMinutes(-8)
            },
            new RoomEvent {
                Id = Person_1_HighFiveEventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.HighFive,
                DateTime = DateTime.Now.AddMinutes(-7)
            },
            new RoomEvent {
                Id = Person_0_HighFiveEventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[0].Id,
                Type = RoomEventType.HighFive,
                DateTime = DateTime.Now.AddMinutes(-6)
            },
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[0].Id,
                Type = RoomEventType.LeaveTheRoom,
                DateTime = DateTime.Now.AddMinutes(-5)
            },
            new RoomEvent {
                Id = Person_1_CommentEventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.Comment,
                DateTime = DateTime.Now.AddMinutes(-4)
            },
            new RoomEvent {
                Id = Person_1_Comment2EventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.Comment,
                DateTime = DateTime.Now.AddMinutes(-3)
            },
            new RoomEvent {
                Id = Person_1_Comment3EventId,
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.Comment,
                DateTime = DateTime.Now.AddMinutes(-2)
            },
            new RoomEvent {
                Id = Guid.NewGuid(),
                RoomId = RoomsMock.Room_1.Id,
                PersonId = PersonsMock.Persons[1].Id,
                Type = RoomEventType.LeaveTheRoom,
                DateTime = DateTime.Now.AddMinutes(-1)
            }
        };
    }
}

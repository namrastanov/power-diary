﻿using ChatApp.Data.Models;
using System;
using System.Collections.Generic;

namespace ChatApp.Data.Mocks
{
    public static class HighFivesMock
    {
        public static IList<HighFive> HighFives = new List<HighFive>
        {
            new HighFive
            {
                Id = Guid.NewGuid(),
                RoomEventId = RoomEventsMock_1.Person_1_HighFiveEventId,
                PersonId = PersonsMock.Persons[0].Id
            },
            new HighFive
            {
                Id = Guid.NewGuid(),
                RoomEventId = RoomEventsMock_2.Person_1_HighFiveEventId,
                PersonId = PersonsMock.Persons[0].Id
            },
            new HighFive
            {
                Id = Guid.NewGuid(),
                RoomEventId = RoomEventsMock_2.Person_0_HighFiveEventId,
                PersonId = PersonsMock.Persons[1].Id
            }
        };
    }
}

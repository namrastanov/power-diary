using Microsoft.Extensions.DependencyInjection;
using Xunit;
using ChatApp.Data.Queries;
using ChatApp.Test.Data;
using ChatApp.Data.Mocks;
using System;

namespace ChatApp.Test
{
    public class RoomQueryTest
    {
        private readonly IRoomEventQuery _roomEventQuery;
        

        public RoomQueryTest()
        {
            var serviceProvider = DependencyInjector.GetServiceProvider();

            _roomEventQuery = serviceProvider.GetService<IRoomEventQuery>();
        }

        [Fact]
        public void GetAllRoomEvents()
        {
            var roomEvents = _roomEventQuery.GetAll();

            Assert.NotNull(roomEvents);
        }

        [Fact]
        public void GetByIdRoomEvent()
        {
            var testRoomEvent = RoomEventsMock_1.RoomEvents[0];
            var roomEvent = _roomEventQuery.GetById(testRoomEvent.Id);

            Assert.NotNull(roomEvent);
            Assert.Equal(roomEvent.Type, testRoomEvent.Type);
        }

        [Fact]
        public void GetByIdRoomEvent_NotFound()
        {
            var roomEvent = _roomEventQuery.GetById(Guid.NewGuid());

            Assert.Null(roomEvent);
        }
    }
}

﻿using ChatApp.Core;
using ChatApp.Data;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ChatApp.Test.Data
{
    internal static class DependencyInjector
    {
        public static IServiceProvider GetServiceProvider()
        {
            return new ServiceCollection()
                .InitDataDependencies()
                .InitCoreDependencies()
                .BuildServiceProvider();
        }
    }
}
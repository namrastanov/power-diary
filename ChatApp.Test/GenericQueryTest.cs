﻿using Microsoft.Extensions.DependencyInjection;
using Xunit;
using ChatApp.Data.Queries;
using ChatApp.Test.Data;
using ChatApp.Data.Models;

namespace ChatApp.Test
{
    public class GenericQueryTest
    {
        private readonly IGenericQuery<Comment> _commentQuery;
        private readonly IGenericQuery<Person> _personQuery;
        private readonly IGenericQuery<HighFive> _highFiveQuery;

        public GenericQueryTest()
        {
            var serviceProvider = DependencyInjector.GetServiceProvider();

            _commentQuery = serviceProvider.GetService<IGenericQuery<Comment>>();
            _personQuery = serviceProvider.GetService<IGenericQuery<Person>>();
            _highFiveQuery = serviceProvider.GetService<IGenericQuery<HighFive>>();
        }

        [Fact]
        public void GetAllComments()
        {
            var roomEvents = _commentQuery.GetAll();

            Assert.NotNull(roomEvents);
        }

        [Fact]
        public void GetAllPersons()
        {
            var roomEvents = _personQuery.GetAll();

            Assert.NotNull(roomEvents);
        }

        [Fact]
        public void GetAllHighFives()
        {
            var roomEvents = _highFiveQuery.GetAll();

            Assert.NotNull(roomEvents);
        }
    }
}

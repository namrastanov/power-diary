﻿namespace ChatApp.Enums
{
    public enum GranularityTypes
    {
        Default, Minutes, Hours
    }
}

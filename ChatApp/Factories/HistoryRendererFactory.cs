﻿using ChatApp.Core.ReadModels;
using ChatApp.Enums;
using ChatApp.Renderers.HistoryRenderer;
using System;
using System.Collections.Generic;

namespace ChatApp.Renderers.Factories
{
    public class HistoryRendererFactory
    {
        public static Dictionary<GranularityTypes, Func<RoomHistory, IHistoryRenderer>> Creators =
            new Dictionary<GranularityTypes, Func<RoomHistory, IHistoryRenderer>>
            {
                { GranularityTypes.Default, (history) => new DefaultHistoryRenderer { History = history } },
                { GranularityTypes.Minutes, (history) => new DefaultHistoryRenderer { History = history } },
                { GranularityTypes.Hours, (history) => new HourlyHistoryRenderer { History = history } },
            };
    }
}

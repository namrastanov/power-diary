﻿using ChatApp.Core.ReadModels;
using ChatApp.Renderers.EventRenderer;
using System;
using System.Collections.Generic;

namespace ChatApp.Renderers.Factories
{
    public class EventRendererFactory
    {
        public static Dictionary<Type, Func<RoomEventReadModel, IEventRenderer>> Creators =
            new Dictionary<Type, Func<RoomEventReadModel, IEventRenderer>>
            {
                { typeof(EnterEvent), (enterEvent) => new EnterEventRenderer { Event = enterEvent as EnterEvent } },
                { typeof(LeaveEvent), (leaveEvent) => new LeaveEventRenderer { Event = leaveEvent as LeaveEvent } },
                { typeof(CommentEvent), (commentEvent) => new CommentEventRenderer { Event = commentEvent as CommentEvent } },
                { typeof(HighFiveEvent), (highFiveEvent) => new HighFiveEventRenderer { Event = highFiveEvent as HighFiveEvent } },
            };
    }
}

﻿using ChatApp.Renderers.Factories;
using ChatApp.Renderers.HistoryRenderer.Models;
using System.Globalization;
using System.Text;

namespace ChatApp.Renderers.HistoryRenderer
{
    class HourlyHistoryRenderer : HistoryRenderer
    {
        public override string Render()
        {
            var result = new StringBuilder();
            result.AppendLine("Granularity: Hourly");

            var currentHistoryItem = new HourlyHistoryItem();
            var previousTimeKey = "";
            var isFirstIteration = true;
            foreach (var roomEvent in History.RoomEvents)
            {
                var eventRenderer = EventRendererFactory.Creators[roomEvent.GetType()](roomEvent);
                var currentTimeKey = roomEvent.DateTime.ToString("hh tt", CultureInfo.InvariantCulture);

                if (previousTimeKey != currentTimeKey)
                {
                    AppendHistoryItem(result, currentHistoryItem, isFirstIteration);

                    result.AppendLine($"{currentTimeKey}:");
                    currentHistoryItem = new HourlyHistoryItem();
                    previousTimeKey = currentTimeKey;
                }

                eventRenderer.UpdateHourlyHistoryItem(currentHistoryItem);
                isFirstIteration = false;
            }
            AppendHistoryItem(result, currentHistoryItem);

            return result.ToString();
        }

        private void AppendHistoryItem(StringBuilder sb, HourlyHistoryItem historyItem, bool isFirstIteration = false)
        {
            if (isFirstIteration) return;
            var enteredPersonsCount = historyItem.EnteredPersons.Count;
            sb.AppendLine($"---{enteredPersonsCount} {PersonText(enteredPersonsCount)} entered");
            sb.AppendLine($"---{historyItem.LeftPersons.Count} left");

            var highFiveFromCount = historyItem.HighFivePersonsFrom.Count;
            var highFiveToCount = historyItem.HighFivePersonsTo.Count;
            sb.AppendLine($"---{highFiveFromCount} {PersonText(highFiveFromCount)} high-fived {highFiveToCount} other {PersonText(highFiveToCount)}");

            sb.AppendLine(@$"---{historyItem.CommentsCount} comment{(historyItem.CommentsCount > 0 ? "s" : "")}");
        }

        private static string PersonText(int count)
        {
            return count > 1 ? "people" : "person";
        }
    }
}

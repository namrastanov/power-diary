﻿using ChatApp.Core.ReadModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatApp.Renderers.HistoryRenderer.Models
{
    public class HourlyHistoryItem
    {
        public IList<PersonReadModel> EnteredPersons { get; set; } = new List<PersonReadModel>();
        public IList<PersonReadModel> LeftPersons { get; set; } = new List<PersonReadModel>();
        public IList<PersonReadModel> HighFivePersonsFrom { get; set; } = new List<PersonReadModel>();
        public IList<PersonReadModel> HighFivePersonsTo { get; set; } = new List<PersonReadModel>();
        public int CommentsCount { get; set; }
    }
}

﻿using ChatApp.Core.ReadModels;
using ChatApp.Renderers.Factories;
using System.Globalization;
using System.Text;

namespace ChatApp.Renderers.HistoryRenderer
{
    class DefaultHistoryRenderer: HistoryRenderer
    {
        public override string Render()
        {
            var result = new StringBuilder();
            result.AppendLine("Granularity: Minute by minute");

            foreach (var roomEvent in History.RoomEvents)
            {
                var renderer = EventRendererFactory.Creators[roomEvent.GetType()](roomEvent);
                result.AppendLine($"{roomEvent.DateTime.ToString("hh:mm tt", CultureInfo.InvariantCulture)}: {renderer.Render()}");
            }

            return result.ToString();
        }
    }
}

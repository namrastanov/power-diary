﻿using ChatApp.Core.ReadModels;

namespace ChatApp.Renderers.HistoryRenderer
{
    public abstract class HistoryRenderer: IHistoryRenderer
    {
        public RoomHistory History { get; set; }

        public abstract string Render();
    }
}

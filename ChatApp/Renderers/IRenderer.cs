﻿namespace ChatApp.Renderers
{
    public interface IRenderer
    {
        public string Render();
    }
}

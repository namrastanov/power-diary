﻿using ChatApp.Core.ReadModels;
using ChatApp.Renderers.HistoryRenderer.Models;
using System.Linq;

namespace ChatApp.Renderers.EventRenderer
{
    public class EnterEventRenderer: IEventRenderer
    {
        public EnterEvent Event { get; set; }

        public void UpdateHourlyHistoryItem(HourlyHistoryItem historyItem)
        {
            if (!historyItem.EnteredPersons.Any(person => person.Id == Event.Person.Id))
            {
                historyItem.EnteredPersons.Add(Event.Person);
            }
        }

        public string Render()
        {
            return $"{Event.Person?.Name} enters the room";
        }
    }
}

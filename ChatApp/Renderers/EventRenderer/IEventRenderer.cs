﻿using ChatApp.Renderers.HistoryRenderer.Models;

namespace ChatApp.Renderers.EventRenderer
{
    public interface IEventRenderer: IRenderer
    {
        void UpdateHourlyHistoryItem(HourlyHistoryItem historyItem);
    }
}

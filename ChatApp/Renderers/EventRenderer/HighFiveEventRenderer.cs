﻿using ChatApp.Core.ReadModels;
using ChatApp.Renderers.HistoryRenderer.Models;
using System.Linq;

namespace ChatApp.Renderers.EventRenderer
{
    public class HighFiveEventRenderer : IEventRenderer
    {
        public HighFiveEvent Event { get; set; }

        public void UpdateHourlyHistoryItem(HourlyHistoryItem historyItem)
        {
            if (!historyItem.HighFivePersonsFrom.Any(person => person.Id == Event.Person.Id))
            {
                historyItem.HighFivePersonsFrom.Add(Event.Person);
            }

            if (!historyItem.HighFivePersonsTo.Any(person => person.Id == Event.HighFivedPerson.Id))
            {
                historyItem.HighFivePersonsTo.Add(Event.HighFivedPerson);
            }
        }

        public string Render()
        {
            return $"{Event.Person?.Name} high-fives {Event.HighFivedPerson?.Name}";
        }
    }
}

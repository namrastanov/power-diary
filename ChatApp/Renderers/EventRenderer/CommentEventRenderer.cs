﻿using ChatApp.Core.ReadModels;
using ChatApp.Renderers.HistoryRenderer.Models;

namespace ChatApp.Renderers.EventRenderer
{
    public class CommentEventRenderer : IEventRenderer
    {
        public CommentEvent Event { get; set; }

        public void UpdateHourlyHistoryItem(HourlyHistoryItem historyItem)
        {
            historyItem.CommentsCount++;
        }

        public string Render()
        {
            return @$"{Event.Person?.Name} comments: ""{Event.Text}""";
        }
    }
}

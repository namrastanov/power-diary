﻿using ChatApp.Core.ReadModels;
using ChatApp.Renderers.HistoryRenderer.Models;
using System.Linq;

namespace ChatApp.Renderers.EventRenderer
{
    public class LeaveEventRenderer : IEventRenderer
    {
        public LeaveEvent Event { get; set; }

        public void UpdateHourlyHistoryItem(HourlyHistoryItem historyItem)
        {
            if (!historyItem.LeftPersons.Any(person => person.Id == Event.Person.Id))
            {
                historyItem.LeftPersons.Add(Event.Person);
            }
        }

        public string Render()
        {
            return $"{Event.Person?.Name} leaves";
        }
    }
}

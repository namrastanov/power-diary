﻿using ChatApp.Core.Services;
using ChatApp.Data.Mocks;
using ChatApp.Enums;
using ChatApp.Renderers.Factories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace ChatApp
{
    public class Startup
    {
        private static GranularityTypes CurrentGranularityType = GranularityTypes.Default;

        public static void Start(ServiceProvider serviceProvider)
        {
            var roomHistoryService = serviceProvider.GetRequiredService<IRoomHistoryService>();
            var roomHistory = roomHistoryService.GetHistory(RoomsMock.Room_1.Id);

            for (; ; )
            {
                var historyRenderer = HistoryRendererFactory.Creators[CurrentGranularityType](roomHistory);
                Print(historyRenderer.Render());
                PrintMenu();

                if (WaitAndValidateEnteredMenu())
                {
                    PrintGranularitySeparator();
                    continue;
                }

                break;
            }
            Print("Exit");
        }

        private static void PrintMenu()
        {
            Print("Select Granularity:");
            Print("- Enter '1' for Minute by minute");
            Print("- Enter '2' for Hourly");
            Print("- Enter anything else for Exit");
        }

        private static bool WaitAndValidateEnteredMenu()
        {
            if (Enum.TryParse(Console.ReadLine(), out CurrentGranularityType))
            {
                return GranularityTypes.Hours == CurrentGranularityType || GranularityTypes.Minutes == CurrentGranularityType;
            }

            return false;
        }

        private static void PrintGranularitySeparator()
        {
            var seperatorString = "-----------------------------------------------------";
            Print(seperatorString);
            Print(seperatorString);
            Print();
        }

        public static void Print(string text = "")
        {
            Console.WriteLine(text);
        }
    }
}

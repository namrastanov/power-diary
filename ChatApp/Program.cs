﻿using ChatApp.Core;
using ChatApp.Data;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ChatApp
{
    class Program
    {
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += CustomExceptionTrapper;
            Startup.Start(GetServiceProvider());
        }

        private static ServiceProvider GetServiceProvider()
        {
            return new ServiceCollection()
                .InitDataDependencies()
                .InitCoreDependencies()
                .BuildServiceProvider();
        }

        private static void CustomExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.WriteLine("Press Enter to Exit");
            Console.ReadLine();
            Environment.Exit(0);
        }
    }
}
